import logo from './logo.svg';
import './App.css';

function App() {
  const weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
  
  const arrayOfWeekday = weekdays.map((date, position)=>{
  
    return (
      <tr key={position}>
        <td>{date}</td>
      </tr>
    )
  })
  return (
    <div className="container">
      <div className="header">
        <h1 className="title">Assignment W-13</h1>
      </div>
      <hr/>
      <table>
        <thead>
          <tr>
            <th>Weekday</th>
          </tr>
        </thead>
        <tbody className="weekday">
          {arrayOfWeekday}
        </tbody>
      </table>
    </div>
  );
}

export default App;
